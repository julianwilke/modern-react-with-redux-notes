Controlled Components
=====================

1.set up component state inside the constructor

```
constructor(props) {
  super(props);

  this.state = {term: ''}; // this.state may only be called from the constructor
  
  // ...
}
```

2.make the form element always show the value from component state

```
render() {
  //...
  <input value={this.state.term} />
  //...
}

```

3.add onChange handler

```
onInputChange(event) {
	this.setState({term: event.target.value});
}

render() {
  //...
  <input value={this.state.term} onChange={this.onInputChange} />
  //...
}

```

4.add onSubmit handler

```
onFormSubmit(event) {
    event.preventDefault();

    // We need to go an fetch weather data
    this.props.fetchWeather(this.state.term);
    this.setState({term:''});
}

render() {
  //...
  <form onSubmit={this.onFormSubmit}>
  </form>
  //...
}

```

5.since the funtion are used in callbacks and have references to `this` in them bind them so `this` always refers to `SearchBar`

```
constructor(props) {
  // ...
  this.onInputChange = this.onInputChange.bind(this);
  this.onFormSubmit = this.onFormSubmit.bind(this);
}
```

