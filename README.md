- react-router@2.0.0-rc5
- redux-form@4.1.3
- http://redux-form.com/6.4.3/docs/GettingStarted.md/
- [react forms without redux](https://bitbucket.org/julianwilke/modern-react-with-redux-notes/src/019118684ec42303aa4ed3fb63a7a8acdd4772be/react%20forms%20without%20redux.md?at=master&fileviewer=file-view-default)

SETUP
=====

/index.js
---------
1. imports
2. set up `<Router/>`
3. apply Middleware


COMPONENT WORKFLOW
==================
1. Create Component: `./components/posts_index`

2. import component source to `./routes.js`:
    `import PostsIndex from './components/posts_index';`

3. import `./routes.js` to `./index.js`: `import routes from './routes'`;

4. set up child route in `./routes.js`
    ```
      <Route path="/" component={App}>
        <IndexRoute component={PostsIndex} />
        <Route path="greet" component={Greeting} />
      </Route>
    ```

5. set up ActionCreator in `./actions/index.js`
```
    import axios from 'axios';

    export const FETCH_POSTS = 'FETCH_POSTS';

    const ROOT_URL = 'http://reduxblog.herokuapp.com/api';
    const API_KEY = '?key=nohknohk';

    export function fetchPosts() {
      const request = axios.get(`${ROOT_URL}/posts${API_KEY}`);
      
      return {
        type: FETCH_POSTS,
        payload: request
      };
    }
```

6. create reducer in `./reducers/reducer_posts.js` 

7. wire up PostsReducer in rootReducer `./reducers/index.js`

```
import {FETCH_POSTS} from '../actions/index';

const INITIAL_STATE = {
  all: [],
  post: null
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case FETCH_POSTS:
      return { ...state, all: action.payload.data};
    default:
      return state;
  }
}
```

8.Expose ActionCreator as Prop on Component, Promoting it to a Container (since its aware of Redux Dispatches now). fetchPosts ActionCreator is now available as prop on container: `this.props.fetchPosts();`

```
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Link} from 'react-router';

import {fetchPosts} from '../actions/index';

class PostsIndex extends Component {
    // ...
}

export default connect(null, { fetchPosts })(PostsIndex);

```

9.Call ActionCreator from React's lifecycle method `componentDidMount` 

```
componentDidMount() {
  this.props.fetchPosts();
}
```

FORM WORKFLOW
=============

1. set up `FormReducer` as `form` in `rootReducer`

```
import { combineReducers } from 'redux';
import PostsReducer from './reducer_posts';
import { reducer as FormReducer } from 'redux-form';

const rootReducer = combineReducers({
  posts: PostsReducer,
  form: FormReducer
});

export default rootReducer;
```

2.wire the component up to redux-form:

```
import { reduxForm } from 'redux-form';

export default reduxForm({
  form: 'PostsNewForm',
  fields: ['title', 'categories', 'content'],
  validate
}, null, { createPost })(PostsNew);
```

3.create local variables for props inside `render()`:

```
render() {
    const { fields: { title, categories, content }, handleSubmit } = this.props;    
}

```

4.add redux-forms's submit handler to form:

```
<form onSubmit={handleSubmit( this.onSubmit.bind(this) )}>
```

5. hook up action creator or submit callback to `handleSubmit`

```
// ONLY ACTION CREATOR
<form onSubmit={handleSubmit( this.props.createPost )}>

// or as SUBMIT CALLBACK

<form onSubmit={handleSubmit( this.onSubmit.bind(this) )}>

//with

class PostsNew extends Component {
    onSubmit(props) {
    this.props.createPost(props)
    // other stuff
    
  }
}
```

6.connect form elements with redux-form's fields by using the the spread operator for destructuring

```
<input type="text" className="form-control" {...title} />
```

7.write `validate(values)` function outside of component object

```
function validate(values) {
  const errors = {};
  
  if (!values.title) {
    errors.title = 'Enter a username.';
  }
  if (!values.categories) {
    errors.categories = 'Enter categories.';
  }
  if (!values.content) {
    errors.content = 'Enter some content.';
  }
  
  return errors;
}
```

8.set up form element group to show errors (color and text)

```
<div className={`form-group ${title.touched && title.invalid ? 'has-danger' : ''}`}>
          <label>Title</label>
          <input type="text" className="form-control" {...title} />
          <div className="text-help">
            {title.touched ? title.error : ''}
          </div>
        </div>
```

9.For Navigation on Submit

```
import React, { Component, PropTypes } from 'react';

class PostsNew extends Component {
  static contextTypes = {
    router: PropTypes.object
  };

  onSubmit(props) {
    this.props.createPost(props)
      .then(() => {
        this.context.router.push('/');
      });
    
  }

  //..
}

```